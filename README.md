![Pipeline status](https://gitlab.com/grokkingstuff/report-template/badges/master/pipeline.svg)

# Gitlab CI pipeline for LaTeX example

Just push to the gitlab repo and that's it

# Local Compilation

`make clean render`

or 

`make clean render LATEXMK_OPTIONS_EXTRA=-pvc` to keep compiling the pdf when the input files are updated.
